package alauda

import (
    "os"
    "k8s.io/client-go/rest"
    "k8s.io/client-go/kubernetes"
    "k8s.io/apimachinery/pkg/apis/meta/v1"
    "strings"
    "fmt"
)

const (
    DEFAULT_COREFILE_TEMPLATE_PATH = "/etc/coredns/corefile.tmpl"
    DEFAULT_INGRESS_TEMPLATE_PATH = "/etc/coredns/ingress.tmpl"
    DEFAULT_SUFFIX = "alauda.local"
    DEFAULT_BIND_ADDRESS = "0.0.0.0"
)

type Config struct {
    CorefileTemplatePath string
    IngressTemplatePath string
    BindAddress string
    DomainSuffix string
}

type Corefile struct {
    TimeStamp string
    DomainSuffix string
}

type IngressFile struct {
    Domains []string
    Address string
}

func NewConfig() Config {
    config := Config{
        CorefileTemplatePath: os.Getenv("COREFILE_TEMPLATE_PATH"),
        IngressTemplatePath: os.Getenv("INGRESS_TEMPLATE_PATH"),
        BindAddress: os.Getenv("BIND_ADDRESS"),
        DomainSuffix: os.Getenv("DOMAIN_SUFFIX"),
    }

    if config.CorefileTemplatePath == "" {
        config.CorefileTemplatePath = DEFAULT_COREFILE_TEMPLATE_PATH
    }

    if config.IngressTemplatePath == "" {
        config.IngressTemplatePath = DEFAULT_INGRESS_TEMPLATE_PATH
    }

    if config.BindAddress == "" {
        config.BindAddress = getDefaultAddress()
    }

    if config.DomainSuffix == "" {
        config.DomainSuffix = DEFAULT_SUFFIX
    }

    return config
}

func getDefaultAddress() string {
    return DEFAULT_BIND_ADDRESS
}

func getDomainSuffix() (string, error) {
    config, err := rest.InClusterConfig()
    if err != nil {
        return "", err
    }

    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        return "", err
    }
    cm, err := clientset.CoreV1().ConfigMaps("alauda-system").Get("devops-config", v1.GetOptions{})
    if err != nil {
        return "", err
    }
    defaultSuffix := cm.Data["defaultDomain"]
    if defaultSuffix == "" {
        return defaultSuffix, fmt.Errorf("no defaultDomain in configmap devops-config")
    }
    return defaultSuffix, nil
}

func getIngressDomains(suffix string) ([]string, error) {
    config, err := rest.InClusterConfig()
    if err != nil {
        return nil, err
    }

    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        return nil, err
    }

    ingressList, err := clientset.ExtensionsV1beta1().Ingresses("").List(v1.ListOptions{})
    if err != nil {
        return nil, err
    }
    domainMap := map[string]bool{}
    for _, ingress := range ingressList.Items {
        for _, rule := range ingress.Spec.Rules {
            if rule.Host != "" {
                domainMap[rule.Host] = true
            }
        }
    }
    domains := []string{}
    for domain, _ := range domainMap {
        if strings.HasSuffix(domain, suffix) {
            domains = append(domains, strings.TrimSuffix(domain, suffix))
        }
    }
    return domains, nil
}