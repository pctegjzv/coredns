package alauda

import (
    "html/template"
    "log"
    "os"
    "time"
    "crypto/md5"
    "fmt"
    "io"
)

// Run is alauda main() function.
func Run() {
    for {
        time.Sleep(5 * time.Second)
        config := NewConfig()

        it, err := template.New("ingress.tmpl").ParseFiles(config.IngressTemplatePath)
        if err != nil {
            log.Printf("failed to parse template ingress.tmpl %v \n", err)
        }
        if err != nil {
            log.Printf("failed to write corefile %v \n", err)
        }
        writer, _ := os.Create("/etc/coredns/ingress.tmp")
        suffix, err := getDomainSuffix()
        if err != nil {
            log.Printf("failed to get default suffix from configmap %v", err)
        }
        if suffix != "" {
            config.DomainSuffix = suffix
        }

        domains, err := getIngressDomains(config.DomainSuffix)
        if err != nil {
            log.Printf("can not get ingress domains %v \n", err)
            continue
        }
        err = it.Execute(writer, IngressFile{Domains: domains, Address: config.BindAddress})
        if err != nil {
            log.Printf("failed to write ingress %v \n", err)
        }
        writer.Sync()
        writer.Close()

        if !sameFiles("/etc/coredns/ingress", "/etc/coredns/ingress.tmp") {
            os.Rename("/etc/coredns/ingress.tmp", "/etc/coredns/ingress")

            ct, err := template.New("corefile.tmpl").ParseFiles(config.CorefileTemplatePath)
            if err != nil {
                log.Printf("failed to parse template Corefile.tmpl %v \n", err)
            }
            writer, _ := os.Create("/etc/coredns/corefile.tmp")
            err = ct.Execute(writer, Corefile{TimeStamp: time.Now().String(), DomainSuffix: config.DomainSuffix})
            writer.Sync()
            writer.Close()

            os.Rename("/etc/coredns/corefile.tmp", "/etc/coredns/corefile")
        }

    }
}

func fileMd5(file string) (string, error) {
    f, err := os.Open(file)
    if err != nil {
        log.Printf("failed to open file %s %v \n", file, err)
        return "", err
    }
    md5h := md5.New()
    io.Copy(md5h, f)
    return fmt.Sprintf("%x", md5h.Sum(nil)), nil
}

func sameFiles(file1, file2 string) bool {
    sum1, err := fileMd5(file1)
    if err != nil {
        return false
    }
    sum2, err := fileMd5(file2)
    if err != nil {
        return false
    }

    return sum1 == sum2
}