package main

//go:generate go run directives_generate.go

import (
	"github.com/coredns/coredns/coremain"

	// Plug in CoreDNS
	_ "github.com/coredns/coredns/core/plugin"
	"github.com/coredns/coredns/alauda"
)

func main() {
	go alauda.Run()
	coremain.Run()
}
