FROM alpine:latest

# Only need ca-certificates & openssl if want to use DNS over TLS (RFC 7858).
RUN apk --no-cache add bind-tools ca-certificates openssl && update-ca-certificates

ADD coredns /coredns
ADD alauda/corefile.tmpl /etc/coredns/corefile.tmpl
ADD alauda/corefile.empty /etc/coredns/corefile
ADD alauda/ingress.tmpl /etc/coredns/ingress.tmpl
ADD alauda/ingress.empty /etc/coredns/ingress

EXPOSE 53 53/udp
CMD ["/coredns", "-conf", "/etc/coredns/corefile"]
